/*global require, applicationContext*/

(function() {
  "use strict";

  // Initialise a new FoxxApplication.
  var Controller = require("org/arangodb/foxx").Controller,
  controller = new Controller(applicationContext);

}());
